// ==UserScript==
// @name         Basic JSPerf
// @namespace    https://github.com/keyCat/userscripts/jsperf
// @version      0.1
// @description  Measure performance of a JS function within n cylces (def. 50,000). Attaches jsperf(fn, cycles) function to window object. Uses performance.now() and for
// @author       Ilya Gorbachevsky
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function(window) {
    'use strict';
    if (window.performance) {
        window.jsperf = jsperf;
    } else {
       console.warn('Your browser doesn\'t provide the Performance API interface, jsperf() will not be exported: https://developer.mozilla.org/en-US/docs/Web/API/Performance');
    }
    function jsperf(fn, cycles) {
        var performance = window.performance;
        var name = fn.name ? fn.name : 'function';
        var t0, t1;
        cycles = parseInt(cycles, 10) > 0 ? parseInt(cycles) : 50000;
        t0 = performance.now();
        for (var i = 0; i < cycles; i++) {
            fn();
        }
        t1 = performance.now();
        console.info('jsperf: %s took %s ms within %s cycle(s)', name, t1 - t0, cycles);
        return t1 - t0;
    }
})(window);